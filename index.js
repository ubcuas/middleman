const express = require('express');
const fs = require('fs');
const app = express();
const multer = require('multer');
const path = require('path')

app.use('/', express.static(path.join(__dirname, 'public')))

let i = 0;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public')
    },
    filename: function (req, file, cb) {
        cb(null, `${i}.jpg`)
    }
})

var upload = multer({ storage: storage });

const getLargestFileIndex = () => {
    return new Promise((resolve, reject) => {
        fs.readdir(__dirname + "/public", (err, file_names) => {
            let k = 0;
            if (err) reject(err)
            else {
                for (let file_name in file_names) {
                    if (file_name > k) k = +file_name;
                }
                resolve(k);
            }
        });
    });
}

app.post('/imageUpload', upload.single("image"), (req, res, next) => {
    const file = req.file
    if (!file) {
        res.status(400)
    } else {
        getLargestFileIndex().then(largestIndex => {
            fs.writeFile(__dirname + "/public/" + (largestIndex + 1) + ".jpg", req.file, (err) => {
                if (err) throw err;
                console.log("Image received!")
                res.status(200).send('File uploaded!');
            });
        })
    }
});

const http = require('http').Server(app);

const io = require('socket.io')(http, {
    cors: {
        origin: '*'
    }
});

// function uuidv4() {
//     return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
//         var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
//         return v.toString(16);
//     });
// }

// const wsserver = new WSServer(uuidv4(), io);

io.on('connection', (socket) => {
    console.log('client connected');
    socket.on('controlsUpdate', (data) => {
        console.log(data);
        // const { ... } = data;
        // ...
    }).on('gimbalUpdate', (data) => {
        console.log(data);
        // const { ... } = data;
        // ...
    }).on('disconnect', () => {
        console.log('client disconnected');
    });
});

http.listen(4000, () => {
    console.log('listening on *:4000');
});
