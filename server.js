const sockets = []

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

class WSServer {
    constructor(roomID, io) {
        this.roomID = roomID;
        this.io = io;
    };

    controlsUpdate() {
        this.io.in(this.roomID).emit("controlsUpdate", {
            //TODO: structure this event somehow
        });
    }

    //  Just provisioning
    // imageUpdate() {
    //     this.io.in(this.roomID).emit("imageUpdate", {
    //         //TODO: structure this event somehow
    //     });
    // }

    randElement(list) {
        return list[randBetween(0, list.length)]
    }

    clientIndex(socketID) {
        return this.players.findIndex(p => p.socketID === socketID);
    }

    clientJoin(socket) {
        socket.join(this.roomID)
        sockets.push(socket)
    }

    clientLeave(socket) {
        socket.leave(this.roomID)
    }

}

module.exports = WSServer